<?php

require_once('Factory.php');

$factory = new Factory();
/*
  circulo
*/
$circle = $factory->getShape('circulo');
$traingle = $factory->getShape('triangulo');
$square = $factory->getShape('cuadrado');
?>
<!doctype html>
<html>
  <head>
    <title>Figuras</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  </head>
  <body>
    <div class="container">
      <table class="table table-striped">
        <thead>
          <th>Figura</th>
          <th>Superficie</th>
          <th>Altura</th>
          <th>Base</th>
          <th>Diametro</th>
        </thead>
        <tbody>
          <tr>
            <td><?=$circle->getFigura();?></td>
            <td><?=$circle->getSuperficie();?></td>
            <td><?=$circle->getAltura();?></td>
            <td><?=$circle->getBase();?></td>
            <td><?=$circle->getDiametro();?></td>
          </tr>
          <tr>
            <td><?=$square->getFigura();?></td>
            <td><?=$square->getSuperficie();?></td>
            <td><?=$square->getAltura();?></td>
            <td><?=$square->getBase();?></td>
            <td><?=$square->getDiametro();?></td>
          </tr>
          <tr>
            <td><?=$traingle->getFigura();?></td>
            <td><?=$traingle->getSuperficie();?></td>
            <td><?=$traingle->getAltura();?></td>
            <td><?=$traingle->getBase();?></td>
            <td><?=$traingle->getDiametro();?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
</html>
