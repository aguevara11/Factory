<?php
require_once('Shape.php');
class Square implements Shape {

	function __construct($figura)
	{
		$this->figura = $figura;
	}

	public function getDiametro()
	{
		return null;
	}

	public function getFigura(){
		return $this->figura;
	}

	public function getBase(){
		return 'superficie / altura';
	}

	public function getAltura(){
		return 'superficie / base';
	}

	public function getSuperficie(){
		return 'base x base';
	}

}
