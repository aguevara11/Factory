<?php
require_once('Shape.php');
class Circle implements Shape {

	function __construct($figura)
	{
		$this->figura = $figura;
	}

	public function getDiametro()
	{
		return "radio * 2";
	}

	public function getFigura(){
		return $this->figura;
	}

	public function getBase(){
		return null;
	}

	public function getAltura(){
		return null;
	}

	public function getSuperficie(){
		return "pi * radio al cuadrado";
	}

}
