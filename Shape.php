<?php
/**
*
*/
interface Shape {
	function getFigura();
	function getSuperficie();
	function getBase();
	function getAltura();
	function getDiametro();
}
