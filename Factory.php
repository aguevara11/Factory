<?php

/**
*
*/
require_once('Circle.php');
require_once('Triangle.php');
require_once('Square.php');
class Factory
{
	public function getShape($shape){
	   if($shape == null){
         return null;
      }
      if($shape=='circulo'){
         return new Circle($shape);

      } else if($shape=='triangulo'){
         return new Triangle($shape);

      } else if($shape=='cuadrado'){
         return new Square($shape);
      }
	}
}
