<?php
require_once('Shape.php');
class Triangle implements Shape {

	function __construct($figura)
	{
		$this->figura = $figura;
	}

	public function getDiametro()
	{
		return null;
	}

	public function getFigura(){
		return $this->figura;
	}

	public function getBase(){
		return '(area x 2) / altura';
	}

	public function getAltura(){
		return '(base x  ½) / superficie';
	}

	public function getSuperficie(){
		return '(base x altura) / 2';
	}

}
